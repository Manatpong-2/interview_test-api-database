# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Dependency Requirements ###

* PHP
* PHP Composer
* MySQL

### Instructions ###

* Clone this repository
* Config database in the file .env and change DB_DATABASE={your database name}
* Run command prompt in the project
* Type "php artisan migrate" to migrate database
* Copy files .htaccess and index.php from "/public/" and paste to your "root" web service
* Edit file "index.php" at line "$app = require __DIR__.'{dir}';" change the directory to your project in the path, example:"/