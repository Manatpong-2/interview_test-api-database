<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //
    public $des;
    public $imagePath = './images/';
    // ================ For Common Function ===================
    public function resp_obj($results, $status) {
        if($status == 200) {
            $results[0]->data = json_decode($results[0]->data);
            $results = $results[0];
            $results->status = (int)$results->status;
        } else if($status == 401) {
            $results = new \stdClass;
            $results->status = -1;
            $results->msg = 'Unauthorized';
            $results->data = new \stdClass;
        } else {
            if (is_null($results)) {
                $results = new \stdClass;
                $results->status = -1;
                $results->msg = 'Invalid parameters';
                $results->data = new \stdClass;
            }
            
        }
        return response()->json($results, $status);
    } 

    public function strIsOrdering($str = '') {
        if(strlen($str) > 1) {
            $str_arr = str_split($str);
            $last_ascii = null;
            $asc = true;
            $desc = true;
            foreach ($str_arr as $chr) {
                # code...
                if($last_ascii) {
                    if(ord($chr) == $last_ascii+1 && $asc) {
                       $desc = false; 
                    } else if(ord($chr) == $last_ascii-1 && $desc) {
                        $asc = false;
                    } else {
                        return false;
                    }
                }
                $last_ascii = ord($chr);
            }

            if($asc || $desc) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }
}
