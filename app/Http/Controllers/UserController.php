<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function create(Request $request)
    {  
        $validate_params = [
            'username' => 'required|string|min:6|max:12|regex:/(^(\w+)$)/u',
            'password' => 'required|string|min:6|max:12',
            'firstname' => 'required|string|min:4|max:40',
            'lastname' => 'required|string|min:4|max:40',
            'profile_img' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:min_width=100,min_height=100,max_width=1000,max_height=1000',
            'tmp_img_name' => 'string',
        ];
        $params = $this->validate($request,$validate_params);

        #validate ordering characters password
        if(!$this->strIsOrdering($request->input('password'))) {
            $has_img = 0;
            if($request->hasFile('profile_img')) {
                $has_img = 1;
            }
            $results = app('db')->select('call user_create(?,?,?,?,?);',[$request->input('username'),$request->input('password'),$request->input('firstname'),$request->input('lastname'),$has_img]);
            if($results[0]->status == 1) {
                $data = json_decode($results[0]->data);
                if($request->hasFile('profile_img')) {
                    $request->file('profile_img')->move($this->imagePath, $data->user_id.'.jpg');
                }
                $tmp_file = 'images/'.$request->input('tmp_img_name');
                if (file_exists($tmp_file) && $request->input('tmp_img_name')) {
                    unlink($tmp_file);
                } 
            }
            return $this->resp_obj($results, 200);
        } else {
            return response()->json(['status' => 1,'msg' => 'That password is insecure','data' => new \stdClass], 422);
        }
        
    }

    public function update(Request $request, $user_id)
    {
        $validate_params = [
            'old_password' => 'string|min:6|max:12',
            'new_password' => 'string|min:6|max:12',
            'firstname' => 'string|min:4|max:40',
            'lastname' => 'string|min:4|max:40',
            'profile_img' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:min_width=100,min_height=100,max_width=1000,max_height=1000',
            'tmp_img_name' => 'string',
        ];
        $params = $this->validate($request,$validate_params);

        if(!$this->strIsOrdering($request->input('password'))) {
            $has_img = 0;
            if($request->hasFile('profile_img')) {
                $has_img = 1;
            }
            $results = app('db')->select('call user_update(?,?,?,?,?,?);',[$user_id,$request->input('old_password'),$request->input('new_password'),$request->input('firstname'),$request->input('lastname'),$has_img]);
            if($results[0]->status == 1) {
                if($request->hasFile('profile_img')) {
                    $request->file('profile_img')->move($this->imagePath, $user_id.'.jpg');
                }
                $tmp_file = 'images/'.$request->input('tmp_img_name');
                if (file_exists($tmp_file) && $request->input('tmp_img_name')) {
                    unlink($tmp_file);
                } 
            }
            return $this->resp_obj($results, 200);
        } else {
            return response()->json(['status' => -1,'msg' => 'That password is insecure','data' => new \stdClass], 422);
        }
    }

    public function uploadImage(Request $request)
    {   
        $validate_params = [
            'profile_img' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:10240|dimensions:max_width=1000,max_height=1000',
        ];
        $params = $this->validate($request,$validate_params);
        $filename = 'tmp'.date("Ymdhis").'.jpg';
        if($request->file('profile_img')->move($this->imagePath, $filename)) {
            $data = new \stdClass;
            $data->filename = $filename;
            return response()->json(['status' => 1,'msg' => 'Upload file successful','data' => $data], 200); 
        } else {
            return response()->json(['status' => -1,'msg' => 'Input file is invalid','data' => new \stdClass], 422); 
        }
        
    }

    public function get(Request $request, $user_id = null)
    {   
        $results = app('db')->select('call user_get(?);',[$user_id]);
        return $this->resp_obj($results, 200);
        
    }
}
