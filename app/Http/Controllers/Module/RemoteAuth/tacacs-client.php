<?php
/**
* Tacacs_Client is a sample TACACS+ client for authentication purposes.
*
* This source code is provided as a demostration for TACACS+ authentication.
*
* PHP Version 5
*
* @category Authentication
* @package  TacacsPlus
* @author   Martín Claro <martin.claro@gmail.com>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     https://gitlab.com/martinclaro
*/

error_reporting(E_ALL);
set_time_limit(0);
ob_implicit_flush();

require_once __DIR__ . '/tacacs/vendor/autoload.php';

use TACACS\Client;
use Monolog\Logger;

function tacacsAccessRequest($sv_addr, $sv_port, $sv_secret, $usr_name, $usr_pwd, $usr_addr) {
    // echo 'access';
    // echo $sv_addr.$sv_port.$sv_secret.$usr_name.$usr_pwd.$usr_addr;
    $tacacs_server_addr         = $sv_addr;
    $tacacs_server_port         = $sv_port;
    $tacacs_server_secret       = $sv_secret;

    $tacacs_user_username       = $usr_name;
    $tacacs_user_password       = $usr_pwd;
    $tacacs_user_port           = 'http';
    $tacacs_user_remote_addr    = $usr_addr;

    $logger = new Logger('tacacs');

    $srv = new Client($logger);
    $srv->setServer(
        $tacacs_server_addr,
        $tacacs_server_port,
        $tacacs_server_secret
    );
    $srv->setTimeout(1.5);
    $res = $srv->authenticate(
        $tacacs_user_username,
        $tacacs_user_password,
        $tacacs_user_port,
        $tacacs_user_remote_addr
    );

    if ($res) {
        // echo "\nAUTHENTICATION SUCCESS!\n\n";
        return true;
    } else {
        // echo "\nAUTHENTICATION FAILED!\n\n";
        return false;
    }

}

// tacacsAccessRequest('192.168.60.169',49,'testing123','manatpong','manatpong','192.168.60.226');

// RUNTIME

// $tacacs_user_remote_addr    = '192.168.197.122';
?>

