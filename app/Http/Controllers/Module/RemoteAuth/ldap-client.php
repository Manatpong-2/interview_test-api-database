<?php 
  function ldapAccessRequest($server,$port,$username, $password, $base_dn) {
    $ldap_dn = "cn=".$username.",".$base_dn;
    // $ldap_dn = "cn=read-only-admin,dc=example,dc=com";
    // $ldap_password = "password;
    $ldap_password = $password;
    try {
      $ldap_con = ldap_connect($server,$port) or die("That LDAP-URI was not parseable");
      ldap_set_option($ldap_con, LDAP_OPT_PROTOCOL_VERSION, 3);
    
      if(ldap_bind($ldap_con, $ldap_dn, $ldap_password)) {
          // echo "Authentication Success!";
          return true;
      } else {
          // echo "Authentication Failed!";
          return false;
      }
    } catch(Exception $e) {
      // echo 'Exception error';
      return false;
    }
  }

  // $test = ldapAccessRequest('ldap.forumsys.com',389,'read-only-admin','password','dc=example,dc=com');
  // echo 'test auth';
  // echo $test;
?>