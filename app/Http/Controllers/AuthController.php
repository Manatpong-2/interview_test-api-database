<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $validate_params = [
            'username' => 'required|string|min:6|max:12',
            'password' => 'required|string|min:6|max:12',
        ];

        $params = $this->validate($request,$validate_params);
        $results = app('db')->select('call auth_login(?,?);',[$request->input('username'),$request->input('password')]);
        return $this->resp_obj($results, 200);
    }

    public function logout(Request $request)
    {
        $validate_params = [
            'token' => 'required|string|min:10|max:100'
        ];

        $params = $this->validate($request,$validate_params);
        $results = app('db')->select('call auth_logout(?);',[$request->input('token')]);
        return $this->resp_obj($results, 200);
    }

    public function authenticate(Request $request)
    {
        $validate_params = [
            'token' => 'required|string|min:10|max:100'
        ];

        $params = $this->validate($request,$validate_params);
        $results = app('db')->select('call auth_check(?);',[$request->input('token')]);
        return $this->resp_obj($results, 200);
    }
}
