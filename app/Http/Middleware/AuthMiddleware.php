<?php

namespace App\Http\Middleware;

use Closure;

class AuthMiddleware extends BaseMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');
        if($token) {
            $results = app('db')->select('call auth_check(?);',[$token]);
            if($results[0]->status == 1) {
                $decode_result = json_decode($results[0]->data);
                // $request->attributes->add(['user_id' => $decode_result->user_id]);
                return $next($request);
            } else {
                return $this->resp_obj($results, 401);
            }
            return $next($request);
        } else {
            return $this->resp_obj(null, 401);
        }
    }
}