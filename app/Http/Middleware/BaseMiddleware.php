<?php

namespace App\Http\Middleware;

use Closure;

class BaseMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function resp_obj($results, $status) {
        if(is_null($results)) {
            $results = new \stdClass;
        } else {
            $results = $results[0];
            $results->status = (int)$results->status;
            $results->data = json_decode($results->data);
        }
        return response()->json($results, $status);
    }
}