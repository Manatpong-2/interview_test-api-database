<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserUpdateProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = <<<EOT
        CREATE DEFINER=`root`@`localhost` PROCEDURE `user_update`(_user_id int, _chk_password varchar(12), _password varchar(12), _firstname varchar(100), _lastname varchar(100), _has_img int)
        BEGIN
            DECLARE errorStatus BOOLEAN DEFAULT FALSE;
            START TRANSACTION;
            BEGIN
                declare _old_password varchar(255);
                declare _hash_password varchar(100);
                declare _result int default -1;
                DECLARE EXIT HANDLER FOR SQLEXCEPTION SET errorStatus = TRUE;
                set _hash_password = md5(_password);
                
                select password into _old_password from users where id = _user_id;
                if (_old_password is null) then
                    begin
                        select -1 as status, concat_ws('','Invalid user id ',_user_id) as msg, '{}' as data;
                    end;
                elseif(_password is not null and ifnull(_old_password != md5(_chk_password),true)) then
                    begin
                        #user change password
                        select -1 as status, 'Update profile failed, old password is incorrect' as msg, '{}' as data;
                    end;
                else
                    begin
                        declare _result int default 1;
                        declare _now timestamp;
                        set _now = current_timestamp;
                        if(_password is not null) then
                            begin
                                #change password
                                if exists(select * from (select hash_password from user_logs where user_id = _user_id order by id desc limit 5) tmp where hash_password = _hash_password) then
                                    begin
                                        set _result = -1;
                                        select -1 as status, 'You used an old password. Choose a new password' as msg, '{}' as data;
                                    end;
                                else
                                    begin
                                        insert into user_logs(user_id,hash_password,updated_at) values(_user_id,_hash_password,_now);
                                    end;
                                end if;
                            end;
                        end if;
                        
                        if(_result = 1) then
                            begin
                                declare _images_path varchar(100);
                                set _images_path = '/images/';
                                if(_has_img = 1) then
                                    begin
                                        update users set img_path = concat(_images_path,_user_id,'.jpg') where id = _user_id;
                                    end;
                                end if;
                                update users set password = ifnull(_hash_password,password), firstname = ifnull(_firstname,firstname), lastname = ifnull(_lastname,lastname),updated_at = _now where id = _user_id;
                                select 1 as status, 'User is updated successfully' as msg, JSON_OBJECT('user_id',_user_id,'username',username,'firstname',firstname,'lastname',lastname,'img_path',img_path,'created_at',created_at,'updated_at',updated_at) as data
                                from users where id = _user_id;
                            end;
                        end if;
                    end;
                end if;
            END;
            
            IF errorStatus = TRUE THEN
                select -1 as status, 'Internal server error' as msg, '{}' as data;
                ROLLBACK;
            ELSE  
                COMMIT;
            END IF;
        END
        EOT;

        DB::unprepared("DROP procedure IF EXISTS user_update");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS user_update");
    }
}
