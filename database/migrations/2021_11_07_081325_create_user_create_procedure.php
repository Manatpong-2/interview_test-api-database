<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCreateProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        $procedure = <<<EOT
        CREATE PROCEDURE `user_create`(_username varchar(12), _password varchar(12), _firstname varchar(100), _lastname varchar(100), _has_img int)
        BEGIN
            DECLARE errorStatus BOOLEAN DEFAULT FALSE;
            START TRANSACTION;
            BEGIN
                DECLARE EXIT HANDLER FOR SQLEXCEPTION SET errorStatus = TRUE;
                if exists(select * from users where username = _username) then
                    begin
                        select -1 as status, concat_ws('','Username ',_username,' is already exists') as msg, '{}' as data;
                    end;
                else
                    begin
                        declare _user_id int;
                        declare _images_path varchar(100);
                        #declare _salt varchar(8);
                        declare _hash_password varchar(100);
                        
                        #set _salt = SUBSTRING(MD5(RAND()) FROM 1 FOR 8);
                        set _images_path = '/images/';
                        set _hash_password = md5(_password);
                        insert into users(username,password,firstname,lastname,created_at,updated_at) values(_username,_hash_password,_firstname,_lastname,current_timestamp,current_timestamp);
                        
                        set _user_id = last_insert_id();
                        if(_has_img = 1) then
                            begin
                                update users set img_path = concat(_images_path,_user_id,'.jpg') where id = _user_id;
                            end;
                        end if;
                        
                        insert into user_logs(user_id,hash_password,updated_at) values(_user_id,_hash_password,current_timestamp);
                        select 1 as status, 'Created user successfully' as msg, JSON_OBJECT('user_id',_user_id,'username',_username,'firstname',_firstname,'lastname',_lastname,'created_at',created_at) as data
                        from users where id = _user_id;
                    end;
                end if;
            END;
            
            IF errorStatus = TRUE THEN
                select -1 as status, 'Internal server error' as msg, '{}' as data;
                ROLLBACK;
            ELSE  
                COMMIT;
            END IF;
        END
        EOT;

        DB::unprepared("DROP procedure IF EXISTS user_create");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS user_create");
    }
}
