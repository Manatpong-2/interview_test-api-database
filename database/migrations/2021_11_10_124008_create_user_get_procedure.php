<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGetProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = <<<EOT
        CREATE PROCEDURE `user_get`(_usr_id int)
        BEGIN
            if(_usr_id is null) then
                begin
                    select 1 as status,'Return user data' as msg, concat_ws('','[',group_concat(json_object('usr_id',id,'username',username,'firstname',firstname,'lastname',lastname,'img_path',img_path,'created_at',created_at,'updated_at',updated_at)),']') as data from users;
                end;
            else
                begin
                    select 1 as status,'Return user data' as msg, json_object('usr_id',id,'username',username,'firstname',firstname,'lastname',lastname,'img_path',img_path,'created_at',created_at,'updated_at',updated_at) as data from users where id = _usr_id;
                end;
            end if;
        END
        EOT;

        DB::unprepared("DROP procedure IF EXISTS user_get");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS user_get");
    }
}
