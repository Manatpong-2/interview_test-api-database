<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthLogoutProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = <<<EOT
        CREATE PROCEDURE `auth_logout`(_token varchar(255))
        BEGIN
            DECLARE errorStatus BOOLEAN DEFAULT FALSE;
            START TRANSACTION;
            BEGIN
                declare _user_id int;
                declare _result int default -1;
                DECLARE EXIT HANDLER FOR SQLEXCEPTION SET errorStatus = TRUE;
                if exists(select * from user_login where token = _token) then
                    begin
                        delete from user_login where token = _token;
                        select 1 as status, 'Logged-out successfully' as msg, '{}' as data;
                    end;
                else
                    begin
                        select -1 as status, 'Incorrect access token' as msg, '{}' as data;
                    end;
                end if;
            End;

        IF errorStatus = TRUE THEN
                select -1 as status, 'Internal server error' as msg, '{}' as data;
                ROLLBACK;
            ELSE  
                COMMIT;
            END IF;
        END
        EOT;

        DB::unprepared("DROP procedure IF EXISTS auth_logout");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS auth_logout");
    }
}
