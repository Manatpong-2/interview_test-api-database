<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthLoginProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = <<<EOT
        CREATE PROCEDURE `auth_login`(_username varchar(12), _password varchar(12))
BEGIN
	DECLARE errorStatus BOOLEAN DEFAULT FALSE;
	START TRANSACTION;
	BEGIN
		declare _user_id int;
		declare _result int default -1;
		DECLARE EXIT HANDLER FOR SQLEXCEPTION SET errorStatus = TRUE;
		select id into _user_id from users where username = _username and password = md5(_password) and deleted_at is null limit 1;
		delete from user_login where token_timeout < current_timestamp;
		if _user_id is null then
			begin
				select -1 as status, 'Incorrect username or password' as msg, '{}' as data;
			end;
		else
			begin
				declare _token_timeout int;
				declare _token varchar(255);
				declare _now timestamp default current_timestamp;
				declare _token_timeout_time varchar(255);
				declare _login_time varchar(255);
				declare _online_time varchar(255);
				set _token_timeout = 60; #set token available timeout
				select token,login_date,timediff_format(login_date,current_timestamp) into _token,_login_time,_online_time from user_login where usr_id = _user_id and token_timeout > current_timestamp;
				set _result = 1;
				set _token_timeout_time = date_add(_now,interval _token_timeout second);
				if _token is null then
					begin
						set _token = md5(concat(lower(_username),rand()));
						insert into user_login(usr_id,token,login_date,token_timeout) value(_user_id,_token,_now,_token_timeout_time);
						select 1 as status, 'Login successfully' as msg, JSON_OBJECT('user_id', convert(_user_id,unsigned), 'username',username,'firstname',firstname,'lastname',lastname,'img_path',img_path,'token', _token, 'token_timeout', _token_timeout_time) as data
						from users where id = _user_id;
					end;
				else
					begin
						update user_login set token_timeout = _token_timeout_time where token = _token;
						select 1 as status, 'Token is updated' as msg, JSON_OBJECT('user_id', convert(_user_id,unsigned), 'username',username,'firstname',firstname,'lastname',lastname,'img_path',img_path,'token', _token, 'token_timeout', _token_timeout_time) as data
						from users where id = _user_id;
					end;
				end if;
			end;
		end if;
	End;

IF errorStatus = TRUE THEN
		select -1 as status, 'Internal server error' as msg, '{}' as data;
		ROLLBACK;
	ELSE  
		COMMIT;
	END IF;
END
EOT;

        DB::unprepared("DROP procedure IF EXISTS auth_login");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS auth_login");
    }
}
