<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimediffFormatFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = <<<EOT
        CREATE FUNCTION `timediff_format`(_start timestamp, _end timestamp) RETURNS varchar(255) CHARSET utf8mb4
        BEGIN
            if(_end > date_add(_start,interval 7 day) or _end < date_add(_start,interval -7 day)) then
                begin
                    return 'Invalid time';
                end;
            else
                begin
                    declare _timediff varchar(255);
                    declare _days int;
                    declare _hours int;
                    declare _minutes int;
                    
                    #set _timediff = CONCAT(
                    set _days = FLOOR(HOUR(TIMEDIFF(_end, _start)) / 24);
                    set _hours = MOD(HOUR(TIMEDIFF(_end, _start)), 24);
                    set _minutes = MINUTE(TIMEDIFF(_end, _start));
                    set _timediff = concat(
                    case when _days = 0 then '' else concat(_days,' days ') end,
                    case when _hours = 0 then '' else concat(_hours,' hours ') end,
                    _minutes,' minutes');
                    RETURN _timediff;
                end;
            end if;
        END
        EOT;
        DB::unprepared("DROP function IF EXISTS timediff_format");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP function IF EXISTS timediff_format");
    }
}
