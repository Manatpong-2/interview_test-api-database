<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthCheckProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = <<<EOT
        CREATE PROCEDURE `auth_check`(_token varchar(255))
        BEGIN
            DECLARE errorStatus BOOLEAN DEFAULT FALSE;
            START TRANSACTION;
            BEGIN
                declare _login_id int;
                DECLARE EXIT HANDLER FOR SQLEXCEPTION SET errorStatus = TRUE;
                select id into _login_id from user_login where token = _token and token_timeout > current_timestamp limit 1;
                delete from user_login where token_timeout <= current_timestamp;
                if _login_id is null then
                    begin
                        select -1 as status, 'Unauthorized, incorrect access token' as msg, '{}' as data;
                    end;
                else
                    begin
                        declare _token_timeout int default 60;
                        declare _token_timeout_time timestamp;
                        set _token_timeout_time = date_add(current_timestamp,interval _token_timeout second);
                        update user_login set token_timeout = _token_timeout_time where id = _login_id;
                        select 1 as status, 'Authentication successful' as msg, JSON_OBJECT('user_id',usr_id,'token',token,'login_date',login_date,'token_timeout',token_timeout) as data
                        from user_login where id = _login_id;
                    end;
                end if;
            END;
            
            IF errorStatus = TRUE THEN
                select -1 as status, 'Internal server error' as msg, '{}' as data;
                ROLLBACK;
            ELSE  
                COMMIT;
            END IF;
        END
        EOT;

        DB::unprepared("DROP procedure IF EXISTS auth_check");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS auth_check");
    }
}
