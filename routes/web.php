<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () {
    return redirect('/web/');
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => '1'], function () use ($router) { #version1

        $router->post('user/image', 'UserController@uploadImage');
        $router->group(['prefix' => 'user'], function () use ($router) {
            $router->get('/', ['middleware' => 'auth', 'uses' => 'UserController@get']);
            $router->get('/{user_id}', ['middleware' => 'auth', 'uses' => 'UserController@get']);
            $router->post('/', 'UserController@create');
            $router->post('/{user_id}', ['middleware' => 'auth', 'uses' => 'UserController@update']);
        });
        $router->group(['prefix' => 'auth'], function () use ($router) {
            $router->post('login', 'AuthController@login');
            $router->delete('logout', 'AuthController@logout');
            $router->post('/', 'AuthController@authenticate');
        });
    });
});

